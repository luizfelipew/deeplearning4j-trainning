package com.deeplearning.utils;

public enum SystemParams {

    LABELS_NUMBER("5"),
    MODEL_FILE("/Users/luizfelipewendtcampos/Documents/git/deeplearning-java/deeplearning4j-trainning/deeplearling4j-training/src/main/resources/CELL_BRANDS.ZIP");

    private final String valor;

    SystemParams(String valorOpcao) {
        valor = valorOpcao;
    }

    public String getValor() {
        return valor;
    }
}
