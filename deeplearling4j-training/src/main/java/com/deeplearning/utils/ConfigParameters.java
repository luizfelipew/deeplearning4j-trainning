package com.deeplearning.utils;

public enum ConfigParameters {
    MODEL_PATH("/Users/luizfelipewendtcampos/Documents/git/deeplearning-java/deeplearning4j-trainning/deeplearling4j-training/src/main/resources/src/main/resources/"),
    MODEL_NAME("CELL_BRANDS.ZIP"),
    MODEL_LABELS_NUMBER("5"),
    TRAIN_DATA_PATH("/Users/luizfelipewendtcampos/Documents/git/deeplearning-java/deeplearning4j-trainning/deeplearling4j-training/src/main/resources/src/main/resources/celulares");

    private final String valor;

    ConfigParameters(String valorOpcao){
        valor = valorOpcao;
    }
    public String getValor(){
        return valor;
    }
}
