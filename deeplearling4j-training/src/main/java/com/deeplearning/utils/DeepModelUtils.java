package com.deeplearning.utils;


import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.text.documentiterator.FileLabelAwareIterator;
import org.deeplearning4j.text.documentiterator.LabelAwareIterator;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;

public class DeepModelUtils {

    static ParagraphVectors paragraphVectors;
    static LabelAwareIterator iterator;
    static TokenizerFactory tokenizerFactory;

    static File getTrainingData() throws IOException {
        return new File(new ClassPathResource("/resources/celulares").getPath());
    }

    public static void main(String[] args) throws IOException {
        final File trainingData = getTrainingData();

        /**
         * Criando um iterator para nosso dataset a ser utilizado no treinamento.
         * Lembre-se que os dados de treinamento devem ser nomeados exatamente como ocorre aqui em :
         * "/poc-dl4j-nlp/resources/celulares".
         * Temos uma pasta para cada marca de celular, cada uma com sua respectiva base de treinamento
         **/
        iterator = new FileLabelAwareIterator.Builder()
            .addSourceFolder(trainingData)
            .build();

        /**
         * Um tokenizer é responsável por efetuar um pre-tratamento nos dados de treinamento.
         * Neste caso estamos deixando somente strings contendo letras e\ou números
         **/
        tokenizerFactory.setTokenPreProcessor(new ProductCommonProcessor());

        /**
         * Definição do modelo da rede neural a ser utilizada para treinamento do modelo de predição
         **/
        paragraphVectors = new ParagraphVectors.Builder()
            .learningRate(0.025)//Tamanho da etapa para cada atualização dos coeficientes utilizados na rede neural
            .minLearningRate(0.001)//É o minimo da taxa de aprendizado.
            //A taxa de aprendizado decai à medida que o número de palavras que você treina diminui.
            //Se a taxa de aprendizado diminuir muito,
            //o aprendizado da rede não será mais eficiente.
            .batchSize(20)//Tamanho do batch individual de processamento
            .epochs(150)//Numero de épocas de treinamento
            .minWordFrequency(1)//Número mínimo de ocorrencias de uma palavra para a mesma compor o modelo
            .layerSize(1000)//Número de dimensões do vetor de saida
            .iterate(iterator)//Iterator contendo dados de treinamento
            .trainWordVectors(true)
            .useAdaGrad(true)
            .tokenizerFactory(tokenizerFactory)
            .build();

        // Inicia o treinamento
        paragraphVectors.fit();

        //Grava o modelo gerado em alguma lozalização
        WordVectorSerializer.writeParagraphVectors(paragraphVectors, ConfigParameters.MODEL_PATH.getValor()
            + ConfigParameters.MODEL_NAME.getValor());


    }

}
