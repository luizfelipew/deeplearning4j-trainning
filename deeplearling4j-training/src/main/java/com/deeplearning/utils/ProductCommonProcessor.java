package com.deeplearning.utils;

import org.deeplearning4j.text.tokenization.tokenizer.TokenPreProcess;

import java.util.regex.Pattern;

public class ProductCommonProcessor implements TokenPreProcess {

    private static final Pattern punctPattern = Pattern.compile("[\\.:,\"\'\\(\\)\\[\\]|/?!;]+");

    @Override
    public String preProcess(final String token) {
        String msgRet = punctPattern.matcher(token).replaceAll("");
        msgRet = msgRet.replaceAll("  ", " ").trim().toLowerCase();
        return msgRet;
    }

}
