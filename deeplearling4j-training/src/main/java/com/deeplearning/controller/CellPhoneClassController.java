package com.deeplearning.controller;

import com.deeplearning.service.ClassifyCellPhoneService;
import com.deeplearning.dto.CellBrand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
@RequestMapping("/api/cellphone-brand")
public class CellPhoneClassController {

    final Logger logger = LoggerFactory.getLogger(CellPhoneClassController.class);

    @Autowired
    private ClassifyCellPhoneService cellPhoneService;

    @GetMapping("/classifycellphone")
    public ResponseEntity<CellBrand> classifyCellPhoneBrand(@RequestParam("product") final String sProductDesc) {
        CellBrand result = new CellBrand();
        if (Objects.isNull(sProductDesc)) {
            return new ResponseEntity<CellBrand>(result, HttpStatus.BAD_REQUEST);
        }

        try {
            result = cellPhoneService.findBrandByCellDesc(sProductDesc);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage(), e);
            return new ResponseEntity<CellBrand>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<CellBrand>(result, HttpStatus.OK);
    }

}
