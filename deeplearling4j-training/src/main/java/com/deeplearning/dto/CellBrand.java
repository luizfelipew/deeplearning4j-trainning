package com.deeplearning.dto;

import java.io.Serializable;

public class CellBrand implements Serializable {
    private static final long serialVersionUID = -1874977111890785699L;

    private String cellPhoneDesc;
    private String brandName;
    private Double brandScore;

    public String getCellPhoneDesc() {
        return cellPhoneDesc;
    }

    public void setCellPhoneDesc(final String cellPhoneDesc) {
        this.cellPhoneDesc = cellPhoneDesc;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(final String brandName) {
        this.brandName = brandName;
    }

    public Double getBrandScore() {
        return brandScore;
    }

    public void setBrandScore(final Double brandScore) {
        this.brandScore = brandScore;
    }

    @Override
    public String toString() {
        return "CellBrand{" +
            "cellPhoneDesc='" + cellPhoneDesc + '\'' +
            ", brandName='" + brandName + '\'' +
            ", brandScore=" + brandScore +
            '}';
    }

}
