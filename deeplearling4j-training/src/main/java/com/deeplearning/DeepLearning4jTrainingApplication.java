package com.deeplearning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeepLearning4jTrainingApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeepLearning4jTrainingApplication.class, args);
	}

}
