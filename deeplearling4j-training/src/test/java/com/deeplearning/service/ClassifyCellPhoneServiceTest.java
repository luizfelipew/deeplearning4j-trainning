package com.deeplearning.service;

import com.deeplearning.dto.CellBrand;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

public class ClassifyCellPhoneServiceTest extends TestCase {

    @Autowired
    private ClassifyCellPhoneService cellPhoneService;

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ClassifyCellPhoneServiceTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(ClassifyCellPhoneServiceTest.class);
    }

    public void testClassifyPhoneTest() {
        CellBrand marca = null;
        try {
            cellPhoneService = new ClassifyCellPhoneService();
            marca = cellPhoneService.findBrandByCellDesc("celular SmartPhone S10");
            if (Objects.isNull(marca)){
                assertFalse(true);
            } else {
                assertTrue(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse(true);
        }
    }

}
